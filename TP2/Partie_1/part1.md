# TP2 pt. 1 : Gestion de service
# I. Un premier serveur web
## 1. Installation

```bash
# paquet httpd
[nioa@web ~]$ sudo dnf install httpd
```

### Démarrer le service Apache

```bash
# le service s'appelle httpd (raccourci pour httpd.service en réalité)
# démarrez le
[nioa@web ~]$ sudo systemctl start httpd

# faites en sorte qu'Apache démarre automatique au démarrage de la machine
[nioa@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

# ouvrez le port firewall nécessaire
[nioa@web ~]$ sudo ss -alnpt
State   Recv-Q  Send-Q   Local Address:Port   Peer Address:Port Process
[...]
LISTEN  0       128                  *:80                *:*     users:(("httpd",pid=2132,fd=4),("httpd",pid=2131,fd=4),("httpd",pid=2130,fd=4),("httpd",pid=2128,fd=4))
[...]
[nioa@web ~]$ sudo firewall-cmd --add-port=80/tcp
success
```

### TEST

```bash
# vérifier que le service est démarré ❌
# vérifier qu'il est configuré pour démarrer automatiquement ⭕
[nioa@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled ⭕; vendor preset: disabled)
   Active: active ❌ (running) since Tue 2021-10-05 14:17:58 CEST; 18min ago
[...]

# vérifier avec une commande curl localhost que vous joignez votre serveur web localement
[nioa@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
[...]
```

## 2. Avancer vers la maîtrise du service
### Le service Apache...

```bash
# donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
[nioa@web ~]$ sudo systemctl enable httpd

# prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume
[nioa@web ~]$ sudo systemctl is-enabled httpd
enabled

# affichez le contenu du fichier httpd.service qui contient la définition du service Apache
[nioa@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

### Déterminer sous quel utilisateur tourne le processus Apache


```bash
# mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé
[nioa@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[...]
User apache
[...]

# utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
[nioa@web ~]$ sudo ps -ef | grep httpd
root        2128       1  0 14:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2129    2128  0 14:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2130    2128  0 14:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2131    2128  0 14:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2132    2128  0 14:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

# vérifiez avec un ls -al le dossier du site (dans /var/www/...)
[nioa@web www]$ ls -al
total 4
drwxr-xr-x.  4 root root   33 Oct  5 14:07 .
drwxr-xr-x. 22 root root 4096 Oct  5 14:07 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
# Executable par tout le monde donc par l'utilisateur apache
```

###  Changer l'utilisateur utilisé par Apache

```bash
# créez le nouvel utilisateur
[nioa@web /]$ sudo useradd dominique -s /sbin/nologin -g apache
[nioa@web /]$ sudo passwd dominique
Changing password for user dominique.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.

# modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
[nioa@web /]$ sudo vim /etc/httpd/conf/httpd.conf
[...]
User apache
[...]

# redémarrez Apache
[nioa@web /]$ sudo systemctl restart httpd

# utilisez une commande ps pour vérifier que le changement a pris effet
[nioa@web /]$ sudo ps -ef | grep httpd
root        2669       1  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dominiq+    2671    2669  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dominiq+    2672    2669  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dominiq+    2673    2669  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dominiq+    2674    2669  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

### Faites en sorte que Apache tourne sur un autre port
```bash
# modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
[nioa@web /]$ sudo cat /etc/httpd/conf/httpd.conf
[...]
Listen 667
[...]

# ouvrez un nouveau port firewall, et fermez l'ancien
[nioa@web /]$ sudo firewall-cmd --add-port=667/tcp
success
[nioa@web /]$ sudo firewall-cmd --remove-port=80/tcp
success

# redémarrez Apache
[nioa@web /]$ sudo systemctl restart httpd

# prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi
[nioa@web /]$ sudo ss -lpnt
State     Recv-Q    Send-Q         Local Address:Port         Peer Address:Port    Process
[...]
LISTEN    0         128                        *:667                     *:*        users:(("httpd",pid=3391,fd=4),("httpd",pid=3390,fd=4),("httpd",pid=3389,fd=4),("httpd",pid=3384,fd=4))

# vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port
[nioa@web /]$ curl localhost:667
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      [...]
```

Fichier: [httpd.conf](./Fichier/I_2_httpd.conf) 

# II. Une stack web plus avancée
## 2. Setup
### Install du serveur Web et de NextCloud sur web.tp2.linux

```bash
# Installing And Configuring Repositories
[nioa@web ~]$ sudo dnf install epel-release
[nioa@web ~]$ sudo dnf update
[nioa@web ~]$ dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[nioa@web ~]$ sudo dnf module list php
[nioa@web ~]$ sudo dnf module enable php:remi-7.4
[nioa@web ~]$ sudo dnf module list php
# Installing Packages
[nioa@web ~]$ sudo dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
# Configuring Packages And Directories
[nioa@web ~]$ sudo systemctl enable httpd
[nioa@web ~]$ sudo mkdir /etc/httpd/sites-available
[nioa@web ~]$ sudo vim /etc/httpd/sites-available/com.web.nextcloud

[nioa@web ~]$ sudo mkdir /etc/httpd/sites-enabled
[nioa@web ~]$ sudo ln -s /etc/httpd/sites-available/com.web.nextcloud /etc/httpd/sites-enabled/
[nioa@web ~]$ sudo mkdir -p /var/www/sub-domains/com.web.nextcloud/html
[nioa@web ~]$ cd /usr/share/zoneinfo/
[nioa@web zoneinfo]$ sudo vim /etc/opt/remi/php74/php.ini
[...]
date.timezone = "Europe/Paris"
[...]
# Installing Nextcloud
[nioa@web ~]$ mkdir nextcloud
[nioa@web ~]$ cd nextcloud/
[nioa@web nextcloud]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[nioa@web nextcloud]$ cd nextcloud/
[nioa@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/com.web.nextcloud/html/
[nioa@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/com.web.nextcloud/html/
[nioa@web nextcloud]$ sudo systemctl restart httpd
```
Fichier: [/etc/httpd/conf/httpd.conf](./Fichier/II_2_A_httpd.conf)
&emsp;&emsp;&emsp; [/etc/httpd/conf/sites-available/web.tp2.linux](./Fichier/II_2_A_web.tp2.linux)

### B. Base de données

```bash
# Install de MariaDB sur db.tp2.linux

[nioa@db ~]$ sudo dnf install mariadb-server
[nioa@db ~]$ sudo systemctl enable mariadb
[nioa@db ~]$ sudo systemctl start mariadb
[nioa@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n]
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n]
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n]
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

# vous repérerez le port utilisé par MariaDB avec une commande ss exécutée sur db.tp2.linux
[nioa@db ~]$ sudo ss -lpnt
State             Recv-Q            Send-Q                        Local Address:Port                         Peer Address:Port            Process
LISTEN            0                 128                                 0.0.0.0:22                                0.0.0.0:*                users:(("sshd",pid=869,fd=5))
LISTEN            0                 80                                        *:3306                                    *:*                users:(("mysqld",pid=28530,fd=22))
LISTEN            0                 128                                    [::]:22                                   [::]:*                users:(("sshd",pid=869,fd=7))
# MariaDb tourne donc sur le port 3306
```



```bash
# Préparation de la base pour NextCloud

[nioa@db ~]$ mysql -u root -p
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

```bash
# Exploration de la base de données

# afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
# On commence par ouvrir le port 3306 sur db.tp2.linux
[nioa@db ~]$ sudo firewall-cmd --add-port=3306/tcp
success
# Puis on se connecte depuis web.tp2.linux
[nioa@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

# utilisez les commandes SQL fournies ci-dessous pour explorer la base
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.003 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)

# trouver une commande qui permet de lister tous les utilisateurs de la base de données
MariaDB [(none)]> SELECT user FROM mysql.user;
+-----------+
| user      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

```bash
# sur votre PC

# modifiez votre fichier hosts (oui, celui de votre PC, de votre hôte)
PS C:\WINDOWS\system32\drivers\etc> cat .\hosts
[...]
10.102.1.11     web.tp2.linux

# avec un navigateur, visitez NextCloud à l'URL http://web.tp2.linux
PS C:\> curl http://web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>
[...]
```

```bash
#  Exploration de la base de données

# connectez vous en ligne de commande à la base de données après l'installation terminée
# déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
MariaDB [(none)]> USE nextcloud;
MariaDB [nextcloud]> SELECT COUNT(*) from INFORMATION_SCHEMA.TABLES
    -> WHERE TABLE_TYPE = 'BASE TABLE' ;
+----------+
| COUNT(*) |
+----------+
|      170 |
+----------+

# NextCloud a créé 170 Tables pusique ma DB était vide tout à l'heure.
```

### Tableau

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | `80/tcp`    | `Aucune`      |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | `3306/tcp`  | `10.102.1.11` |
