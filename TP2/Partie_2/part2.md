# TP2 pt. 2 : Maintien en condition opérationnelle
# I. Monitoring
## 2. Setup
### Setup Netdata
```bash
[nioa@web/db ~]$ sudo su -
[sudo] password for nioa:
[root@web/db ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[...]
  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->
[...]
[root@web/db ~]# exit
logout
```
### Manipulation du service Netdata
```bash
# déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine
[nioa@web/db ~]$ sudo systemctl is-active netdata
active
[nioa@web/db ~]$ sudo systemctl is-enabled netdata
enabled

# déterminer à l'aide d'une commande ss sur quel port Netdata écoute
[nioa@web ~]$ sudo ss -lpnt
State                Recv-Q               Send-Q                             Local Address:Port                               Peer Address:Port               Process
[...]
LISTEN               0                    128                                         [::]:19999                                      [::]:*                   users:(("netdata",pid=2836,fd=6))
# netdata tourne sur le port 19999

# autoriser ce port dans le firewall
[nioa@web/db ~]$ sudo firewall-cmd --add-port 19999/tcp --permanent
success
[nioa@web/db ~]$ sudo firewall-cmd --add-port 19999/tcp
success
```

### Setup Alerting
```bash
[nioa@web/db ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[nioa@web/db ~]$ sudo vim /opt/netdata/etc/netdata/health_alarm_notify.conf
[...]
#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897038502821261342/U0nALn6XeKBcHDX1NKINVuWNAm6gKNHRHdQYFN9bVZczbBZrrxvFED8QEDcO0Rjr-dRm"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"


#------------------------------------------------------------------------------
[...]
[nioa@web/db ~]$ bash -x /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test "sysadmin"
# ça marche (faut me coire sur parole)

[nioa@web/db ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

### Config alerting
```bash
# créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM
[nioa@web/db ~]$ sudo vim /opt/netdata/etc/netdata/health.d/ram-usage.conf
 alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 90
  info: The percentage of RAM being used by the system.

# testez que votre alerte fonctionne
[nioa@web ~]$ stress --vm 2 --timeout 60
stress: info: [2397] dispatching hogs: 0 cpu, 0 io, 2 vm, 0 hdd
stress: info: [2397] successful run completed in 60s
```

# II. Backup
## 2. Partage NFS
### Setup environnement
```bash
# créer un dossier /srv/backup/
[nioa@backup/web/db ~]$ sudo mkdir /srv/backup

# il contiendra un sous-dossier ppour chaque machine du parc
[nioa@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux
[nioa@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux
```
### Setup partage NFS
```bash
[nioa@backup ~]$ sudo dnf -y install nfs-utils
[nioa@backup ~]$ sudo vim /etc/exports
/srv/backup/web.tp2.linux       10.102.1.0/24(rw,no_root_squash)
/srv/backup/db.tp2.linux        10.102.1.0/24(rw,no_root_squash)

[nioa@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

[nioa@backup ~]$ sudo firewall-cmd --add-service=nfs
success
[nioa@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
```

### Setup points de montage sur web.tp2.linux
```bash
# 	Configure NFS Client.
[nioa@web ~]$ sudo dnf install -y nfs-utils
[nioa@web ~]$ sudo mount -t nfs backup:/srv/backup/web.tp2.linux/ /srv/backup
[nioa@web ~]$ df -hT
Filesystem                       Type      Size  Used Avail Use% Mounted on
[...]
backup:/srv/backup/web.tp2.linux nfs4      9.8G  2.4G  7.5G  25% /srv/backup

# vérifier...
# avec une commande mount que la partition est bien montée
[nioa@web ~]$ mount | grep 'backup'
backup:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
/etc/auto.mount on /srv/backup type autofs (rw,relatime,fd=17,pgrp=5121,timeout=300,minproto=5,maxproto=5,direct,pipe_ino=70901)

# avec une commande df -h qu'il reste de la place
[nioa@web ~]$ df -h | grep backup
backup:/srv/backup/web.tp2.linux  9.8G  2.4G  7.5G  25% /srv/backup

# avec une commande touch que vous avez le droit d'écrire dans cette partition
[nioa@backup ~]$ sudo touch /srv/backup/web.tp2.linux/test
[nioa@web ~]$ sudo ls /srv/backup/
test


# faites en sorte que cette partition se monte automatiquement grâce au fichier /etc/fstab
[nioa@web ~]$ sudo vim /etc/fstab
[...]
backup:/srv/backup/web.tp2.linux        /srv/backup     nfs     defaults        0 0
```

### BONUS : partitionnement avec LVM

```bash
# ajoutez un disque à la VM backup.tp2.linux
[nioa@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
[...]
sdb           8:16   0    5G  0 disk
[...]

# utilisez LVM pour créer une nouvelle partition (5Go ça ira)
[nioa@backup ~]$ sudo pvcreate /dev/sdb
[nioa@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize   PFree
  [...]
  /dev/sdb      lvm2 ---    5.00g 5.00g

[nioa@backup ~]$ sudo pvdisplay
 [...]
  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               lpPP2B-lhiv-90Al-D8N2-g8Mb-JGmA-vP3pZ2

[nioa@backup ~]$ sudo vgcreate backup_ext /dev/sdb
[nioa@backup ~]$ sudo vgs
  VG         #PV #LV #SN Attr   VSize   VFree
  backup_ext   1   0   0 wz--n-  <5.00g <5.00g
[...]
[nioa@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup_ext
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               p5qplY-vXce-0VSQ-3j4G-pFh6-4F3f-6V0cmf
[...]

[nioa@backup ~]$ sudo lvcreate -l 100%FREE backup_ext -n backup_ext_lv
[nioa@backup ~]$ sudo lvs
  LV            VG         Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup_ext_lv backup_ext -wi-a----- <5.00g                               
[...]

[nioa@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup_ext/backup_ext_lv
  LV Name                backup_ext_lv
  VG Name                backup_ext
  LV UUID                3P8HL8-AkES-oblf-pqA0-AbLE-9vjZ-cHrHK8
  LV Write Access        read/write
  LV Creation host, time backup.tp2.linux, 2021-10-22 19:34:33 +0200
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
[...]
# monter automatiquement cette partition au démarrage du système à l'aide du fichier /etc/fstab
[nioa@backup ~]$ sudo mkfs -t ext4 /dev/mapper/backup_ext-backup_ext_lv
[...]

[nioa@backup ~]$ sudo mount /dev/mapper/backup_ext-backup_ext_lv /srv/backup/
[nioa@backup ~]$ mount
[...]
/dev/mapper/backup_ext-backup_ext_lv on /srv/backup type ext4 (rw,relatime,seclabel)

[nioa@backup ~]$ df -h
Filesystem                            Size  Used Avail Use% Mounted on
[...]
/dev/mapper/backup_ext-backup_ext_lv  4.9G   20M  4.6G   1% /srv/backup

[nioa@backup ~]$ sudo cat /etc/fstab
[...]
/dev/mapper/backup_ext-backup_ext_lv    /srv/backup     ext4    defaults        0 0

[nioa@backup ~]$ sudo umount /srv/backup
[nioa@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /srv/backup does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/backup              : successfully mounted
```

## 3. Backup de fichiers

### Rédiger le script de backup /srv/tp2_backup.sh

📁 [Fichier /srv/tp2_backup.sh](./Fichiers/tp2_backup.sh)

### Tester le bon fonctionnement

```bash
# exécuter le script sur le dossier de votre choix
[nioa@backup srv]$ sudo mkdir dir1 dir2
[nioa@backup srv]$ sudo touch dir2/hey

[nioa@backup srv]$ sudo ./tp2_backup.sh dir1/ dir2/
dir1/
Archive: tp2_backup_211022_232948.tar.gz successfully created in dir1/.
sending incremental file list
tp2_backup_211022_232948.tar.gz

sent 225 bytes  received 43 bytes  536.00 bytes/sec
total size is 109  speedup is 0.41
Archive: tp2_backup_211022_232948.tar.gz successfully synchronized to dir1/.

# prouvez que la backup s'est bien exécutée
[nioa@backup srv]$ echo $?
0
[nioa@backup srv]$ ls dir1
tp2_backup_211022_232948.tar.gz

# tester de restaurer les données
[nioa@backup dir1]$ sudo tar -xf tp2_backup_211022_232948.tar.gz
[nioa@backup dir1]$ ls
dir2  tp2_backup_211022_232948.tar.gz
[nioa@backup dir1]$ ls dir2/
hey
```

## 4. Unité de service
### A. Unité de service
#### Créer une unité de service pour notre backup
```bash
[nioa@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/dir1  /srv/dir2
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

#### Tester le bon fonctionnement
```bash
# n'oubliez pas d'exécuter sudo systemctl daemon-reload à chaque ajout/modification d'un service
[nioa@web ~]$ sudo systemctl daemon-reload

# essayez d'effectuer une sauvegarde avec sudo systemctl start backup
[nioa@web srv]$ sudo systemctl start tp2_backup

# prouvez que la backup s'est bien exécutée
[nioa@web srv]$ date
Sat Oct 23 17:18:27 CEST 2021
[nioa@web srv]$ ls dir1/
tp2_backup_211023_171825.tar.gz
```

### B. Timer
#### Créer le timer associé à notre tp2_backup.service
```bash
[nioa@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

#### Activez le timer
```bash
# démarrer le timer : sudo systemctl start tp2_backup.timer
[nioa@web srv]$ sudo systemctl start tp2_backup.timer

# activer le au démarrage avec une autre commande systemctl
[nioa@web srv]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.

# le timer est actif actuellement
[nioa@web srv]$ sudo systemctl is-active tp2_backup.timer
active

# qu'il est paramétré pour être actif dès que le système boot
[nioa@web srv]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

#### Tests !
```bash
# Il y a bien une archive par minute et la backup s'execute correctement
[nioa@web srv]$ ls dir1
tp2_backup_211023_225534.tar.gz  tp2_backup_211023_225734.tar.gz  tp2_backup_211023_225945.tar.gz
tp2_backup_211023_225609.tar.gz  tp2_backup_211023_225834.tar.gz
```

### C. Contexte
#### Faites en sorte que...

```bash
# votre backup s'exécute sur la machine web.tp2.linux
[nioa@web ~]$ hostname
web.tp2.linux

# le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans /var/) : 🔺
# la destination est le dossier NFS monté depuis le serveur backup.tp2.linux : 🟢
[nioa@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[...]
ExecStart=/srv/tp2_backup.sh 🟢/srv/backup🟢  🔺/var/www/sub-domains/com.web.nextcloud🔺
[...]

# la sauvegarde s'exécute tous les jours à 03h15 du matin
[nioa@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[...]
OnCalendar=*-*-* 3:15:00
[...]

# prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15
[nioa@web srv]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED      UNIT                         ACTIVATES
Sat 2021-10-23 03:15:00 CEST  3h left  n/a                           n/a          tp2_backup.timer             tp2_backup.service
```
📁 [Fichier /etc/systemd/system/tp2_backup.timer](./Fichiers/tp2_backup.timer)
📁 [Fichier /etc/systemd/system/tp2_backup.service](./Fichiers/tp2_backup.service)

## 5. Backup de base de données
### Création d'un script /srv/tp2_backup_db.sh
📁 [Fichier /srv/tp2_backup_db.sh](./Fichiers/tp2_backup_db.sh)
```bash
# test
[nioa@db ~]$ sudo /srv/tp2_backup_db.sh ./dir_test/ nextcloud
mysqldump: Got errno 32 on write
[nioa@db ~]$ ls dir_test/
db_backup_211024_002430.sql.tar.gz
```

### Restauration
```bash
# tester la restauration de données
[nioa@db dir_test]$ mysql -h localhost -u root --password=nioa nextcloud < db_backup_211024_002430.sql.tar.gz
```

### Unité de service
```bash
# Preuve du bon fonctionnement du service
[nioa@db ~]$ sudo systemctl daemon-reload
[nioa@db ~]$ sudo systemctl start tp2_backup_db
[nioa@db ~]$ sudo systemctl enable tp2_backup_db
[nioa@db ~]$ sudo systemctl is-active tp2_backup_db
active
[nioa@db ~]$ sudo systemctl is-enabled tp2_backup_db
enabled
[nioa@db dir_test]$ ls
db_backup_211024_004902.sql.tar.gz

# Preuve du bon fonctionnement du timer
[nioa@db dir_test]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED    UNIT                         ACTIVATES
[...]
Sun 2021-10-24 03:30:00 CEST  2h 39min left n/a                           n/a       tp2_backup_db.timer          tp2_backup_db.service
[...]
```
📁 [Fichier /etc/systemd/system/tp2_backup_db.timer](./Fichiers/tp2_backup_db.timer)
📁 [Fichier /etc/systemd/system/tp2_backup_db.service](./Fichiers/tp2_backup_db.service)

# III. Reverse Proxy
## 2. Setup simple
### Installer NGINX
```bash
# vous devrez d'abord installer le paquet epel-release avant d'installer nginx
[nioa@front ~]$ sudo dnf install epel-release nginx
```

### Tester !
```bash
# lancer le service nginx
[nioa@front ~]$ sudo systemctl start nginx
[nioa@front ~]$ sudo systemctl is-active nginx
active

# le paramétrer pour qu'il démarre seul quand le système boot
[nioa@front ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[nioa@front ~]$ sudo systemctl is-enabled nginx
enabled

# repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall
[nioa@front ~]$ sudo ss -lpnt
State       Recv-Q      Send-Q            Local Address:Port             Peer Address:Port      Process
[...]
LISTEN      0           128                     0.0.0.0:80                    0.0.0.0:*          users:(("nginx",pid=4201,fd=8),("nginx",pid=4200,fd=8))
[...]
LISTEN      0           128                        [::]:80                       [::]:*          users:(("nginx",pid=4201,fd=9),("nginx",pid=4200,fd=9))
# nginx tourne sur le port 80

# vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC
[nioa@front ~]$ sudo firewall-cmd --add-port 80/tcp
success

PS C:\Users\nicol> curl 10.102.1.14:80


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```

### Explorer la conf par défaut de NGINX
```bash
[nioa@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]
# repérez l'utilisateur qu'utilise NGINX par défaut
user nginx;
[...]

# mettez en évidence ces lignes d'inclusion dans le fichier de conf principal
include /usr/share/nginx/modules/*.conf;

[...]
http {
  
    [...]
    # mettez en évidence ces lignes d'inclusion dans le fichier de conf principal
    include /etc/nginx/conf.d/*.conf;

    # repérez le bloc server {} dans le fichier de conf principal
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]
}
```

### Modifier la conf de NGINX

```bash
# pour que ça fonctionne, le fichier /etc/hosts de la machine DOIT être rempli correctement, conformément à la 📝checklist📝
[nioa@front ~]$ sudo cat /etc/hosts
[...]
10.102.1.11 web.tp2.linux
10.102.1.12 db.tp2.linux
10.102.1.13 backup.tp2.linux

# supprimer le bloc server {} par défaut, pour ne plus présenter la page d'accueil NGINX
[nioa@front ~]$ sudo cat /etc/nginx/nginx.conf
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;
}

# créer un fichier /etc/nginx/conf.d/web.tp2.linux.conf avec le contenu suivant :
[nioa@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 80;
    server_name web.tp2.linux; 
    location / {

        proxy_pass http://web.tp2.linux;
    }
}
```

# IV. Firewalling
## 2. Mise en place
### A. Base de données
#### Restreindre l'accès à la base de données db.tp2.linux
```bash
[nioa@db dir_test]$ sudo firewall-cmd --set-default-zone=drop
success

# seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
[nioa@db dir_test]$ sudo firewall-cmd --new-zone=web --permanent
success
[nioa@db dir_test]$ sudo firewall-cmd --zone=web --add-source=10.102.1.11/32 --permanent
success
[nioa@db dir_test]$ sudo firewall-cmd --zone=web --add-port=3306/tcp --permanent
success

# vous devez aussi autoriser votre accès SSH
[nioa@db dir_test]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[nioa@db dir_test]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[nioa@db dir_test]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
```

#### Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```bash
[nioa@db dir_test]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
web
  sources: 10.102.1.11/32

[nioa@db dir_test]$ sudo firewall-cmd --get-default-zone
drop

[nioa@db dir_test]$ sudo firewall-cmd --list-all --zone=web
web (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@db dir_test]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@db dir_test]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### B. Serveur Web
#### Restreindre l'accès au serveur Web web.tp2.linux
```bash
[nioa@web srv]$ sudo firewall-cmd --set-default-zone=drop
# seul le reverse proxy front.tp2.linux doit accéder au serveur web sur le port 80
[nioa@web srv]$ sudo firewall-cmd --new-zone=proxy --permanent
[nioa@web srv]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.14/32 --permanent
[nioa@web srv]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent

# n'oubliez pas votre accès SSH
[nioa@web srv]$ sudo firewall-cmd --new-zone=ssh --permanent
[nioa@web srv]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[nioa@web srv]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
```

#### Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```bash
[nioa@web srv]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32

[nioa@web srv]$ sudo firewall-cmd --get-default-zone
drop

[nioa@web srv]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@web srv]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@web srv]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### C. Serveur de backup
#### Restreindre l'accès au serveur de backup backup.tp2.linux
```bash
[nioa@backup backup]$ sudo firewall-cmd --set-default-zone=drop
# seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup via NFS
[nioa@backup backup]$ sudo firewall-cmd --new-zone=nfs --permanent
[nioa@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --permanent
[nioa@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.12/32 --permanent

# n'oubliez pas votre accès SSH
[nioa@backup backup]$ sudo firewall-cmd --new-zone=ssh --permanent
[nioa@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.1/32 --permanent
[nioa@backup backup]$ sudo firewall-cmd --zone=nfs --add-port=22/tcp --permanent
```

#### Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```bash
[nioa@backup backup]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32

[nioa@backup backup]$ sudo firewall-cmd --get-default-zone
drop

[nioa@backup backup]$ sudo firewall-cmd --list-all --zone=nfs
nfs (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@backup backup]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@backup backup]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### D. Reverse Proxy
#### Restreindre l'accès au reverse proxy front.tp2.linux
```bash
[nioa@front ~]$ sudo firewall-cmd --set-default-zone drop
# seules les machines du réseau 10.102.1.0/24 doivent pouvoir joindre le proxy
[nioa@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
[nioa@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent

# n'oubliez pas votre accès SSH
[nioa@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[nioa@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[nioa@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
```
#### Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```bash
[nioa@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32

[nioa@front ~]$ sudo firewall-cmd --get-default-zone
drop

[nioa@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[nioa@front ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### E. Tableau récap
#### Rendez-moi le tableau suivant, correctement rempli :

| Machine            | IP            | Service                 | Port ouvert         | IPs autorisées                                    |
|--------------------|---------------|-------------------------|---------------------|---------------------------------------------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | `80/tcp` `22/tcp`   | `10.102.1.14/32` `10.102.1.1/32`                  |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | `3306/tcp` `22/tcp` | `10.102.1.11/32` `10.102.1.1/32`                  |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `22/tcp`            | `10.102.1.11/32` `10.102.1.12/32` `10.102.1.1/32` |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | `80/tcp` `22/tcp`   | `10.102.1.0/24` `10.102.1.1/32`                   |
