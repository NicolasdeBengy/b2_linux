#!/bin/bash
# Database simple backup script
# Nioa - 17/10/2021

destination=$1
db=$2
backup_name=$(date +db_backup_%y%m%d_%H%M%S.sql.tar.gz)

mysqldump -u root --password=nioa ${db} | tar cfz > ${backup_name}
mv ${backup_name} ${destination}
