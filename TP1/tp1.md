# TP1 : (re)Familiarisation avec un système GNU/Linux

## 0. Préparation de la machine

* un accès internet
>ping 8.8.8.8

**node1 :**
```
[nioa@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.2 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.153/18.602/19.052/0.469 ms
```

**node2 :**
```
[nioa@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=38.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=37.9 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 37.920/38.066/38.213/0.243 ms
```
> ping google.com

**node1 :**
```
[nioa@node1 ~]$ ping google.com
PING google.com (216.58.201.238) 56(84) bytes of data.
64 bytes from par10s33-in-f14.1e100.net (216.58.201.238): icmp_seq=1 ttl=114 time=18.5 ms
64 bytes from par10s33-in-f14.1e100.net (216.58.201.238): icmp_seq=2 ttl=114 time=19.8 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.525/19.184/19.843/0.659 ms
```

**node2 :**
```
[nioa@node2 ~]$ ping google.com
PING google.com (216.58.201.238) 56(84) bytes of data.
64 bytes from fra02s18-in-f14.1e100.net (216.58.201.238): icmp_seq=1 ttl=114 time=18.5 ms
64 bytes from fra02s18-in-f14.1e100.net (216.58.201.238): icmp_seq=2 ttl=114 time=20.9 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.465/19.685/20.905/1.220 ms
```


> carte réseau dédiée

**node1 :**
```
[nioa@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b0:38:18 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84336sec preferred_lft 84336sec
    inet6 fe80::a00:27ff:feb0:3818/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```

**node2 :**
```
[nioa@node2 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5f:f2:53 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84539sec preferred_lft 84539sec
    inet6 fe80::a00:27ff:fe5f:f253/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```

> route par défaut

**node1 :**
```
[nioa@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
[...]
```

**node2 :**
```
[nioa@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
[...]
```

> un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only)

**node1 :**
```
[nioa@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.558 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.403 ms

--- 10.101.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1027ms
rtt min/avg/max/mdev = 0.403/0.480/0.558/0.080 ms
```

**node2 :**
```
[nioa@node2 ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.763 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.456 ms

--- 10.101.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.456/0.609/0.763/0.155 ms
```

> les machines doivent posséder une IP statique sur l'interface host-only

**node1 :**
```
[nioa@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

DNS1=1.1.1.1
```

**node2 :**
```
[nioa@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0

DNS1=1.1.1.1
```

* les machines doivent avoir un nom

**node1 :**
```
[nioa@node1 ~]$ hostname
node1.tp1.b2
```

**node2 :**
```
[nioa@node2 ~]$ hostname
node2.tp1.b2
```

* utiliser 1.1.1.1 comme serveur DNS

**node1 :**
```
[nioa@node1 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1
```

**node2 :**
```
[nioa@node2 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1
```

> vérifier avec le bon fonctionnement avec la commande dig

*Ligne qui contient la réponse :* 🔵
*Ligne qui contient l'adresse IP du serveur qui vous a répondu :* 🟢

**node1 :**
```
[nioa@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 51104
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               7267    IN      A       92.243.16.143 🔵

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1) 🟢
;; WHEN: Wed Sep 22 11:51:56 CEST 2021
;; MSG SIZE  rcvd: 53
```

**node2 :**
```
[nioa@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36148
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10738   IN      A       92.243.16.143 🔵

;; Query time: 23 msec
;; SERVER: 1.1.1.1#53(1.1.1.1) 🟢
;; WHEN: Wed Sep 22 11:55:01 CEST 2021
;; MSG SIZE  rcvd: 53
```

* les machines doivent pouvoir se joindre par leurs noms respectifs

**node1 :**
```
[nioa@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2.tp1.b2

[nioa@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.302 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.863 ms

--- node2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1019ms
rtt min/avg/max/mdev = 0.302/0.582/0.863/0.281 ms
```

**node2 :**
```
[nioa@node2 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11 node1.tp1.b2

[nioa@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.318 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.810 ms

--- node1.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1031ms
rtt min/avg/max/mdev = 0.318/0.564/0.810/0.246 ms
```

* le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

**node1 :**
```
[nioa@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**node2 :**
```
[nioa@node2 ~]$ sudo firewall-cmd --list-all
[sudo] password for nioa:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

* Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :

**node1/node2 :**
```
[nioa@node1 ~]$ sudo useradd toto --home /home/toto -s /bin/bash

[nioa@node1 ~]$ cat /etc/passwd
[...]
toto:x:1001:1001::/home/toto:/bin/bash
```

* Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.

**node1/node2 :**
```
[nioa@node1 ~]$ groupadd admins

[nioa@node1 ~]$ sudo cat /etc/sudoers
[...]
%admins ALL(ALL)      ALL
[...]
```
* Ajouter votre utilisateur à ce groupe admins.

```
[nioa@node1 ~]$ sudo usermod -aG admins toto
[nioa@node1 ~]$ groups toto
toto : toto admins
```

### 2. SSH

* il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )

```
PS C:\Users\nicol\.ssh> ssh-keygen -t rsa -b 4096
PS C:\Users\nicol\.ssh> ls


    Répertoire : C:\Users\nicol\.ssh


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        12/10/2020     10:35           3243 id_rsa
-a----        12/10/2020     10:35            748 id_rsa.pub
```

* déposer la clé dans le fichier /home/USER/.ssh/authorized_keys de la machine que l'on souhaite administrer

```
[nioa@node1 temp]$ sudo nano /home/toto/.ssh/authorized_keys
[nioa@node1 temp]$ sudo cat /home/toto/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDWQ1NQDtyoynh08rNno0fCu3JRVwXl/3/GIHndVmtGwJTgVZV/lJM/Nh+fcbNeMvlr/GTKLNUlJVdc54KICJX4EedfEfqkQf6Gdr6p8/zHAdgNhaq0Z63yD4gT/PsoaIdNTlInL7AMddieGgQGJSCp0CQvG/kI8FC43+XnnuuYpEW1EXT9CEtge1OYchdG2vY4nbDQ48IMvIOSKRRdXuLomAoE/YvZK4V46zhzYJmb0ycRUcSj4TVGnaWcKzXrpHDQuTE3YmqOAS66e1JvgKGcxIvRfEnkjJFRFzorGUf1np8gslNsgeKG8LSD1eaGy3txqvSMAYbWFpk5b9/X0bEOSl1IEl8KyVYdPDm9T0AjLPQ0R/ewYeuAZmaTxm1ZOJEPyBh1XMizwOYnwNAh0ZOqQcL3n51KzIzIQgRVX7/VhmvVJQNV4/Mu7O8iM4W9zfwEobHKpj6QCJpuQoGIt9KxsSLCTua2X+v4JoeX3uqoaQsOjuRqkstDfiGNGDLW9lNnPx6kut7mqaH8MHTvtJyckCYmJdEfcv0ZI0jHvwYsEmgnklg9j/GQ18HKFS14AmSu1rj7vZ9Ze0XGtvLJz1wvwXe30kUvM0WFBUf+aGRvRSYUnlPtrv/yXNzCNkUL5jUqm0Bc9PCT/izb0HGJmcaw6JGOvN2ShiXHLQim+6u8lQ== nicol@LAPTOP-OHF77A2B
```
* Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.

```
PS C:\Users\nicol> ssh toto@10.101.1.11

X11 forwarding request failed on channel 0
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sat Sep 25 23:06:52 2021
[toto@node1 ~]$
```
## II. Partitionnement

### 2. Partitionnement

* Utilisez LVM pour :

> agréger les deux disques en un seul volume group

```
[nioa@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for nioa:
  Physical volume "/dev/sdb" successfully created.
[nioa@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
  
[nioa@node1 ~]$ sudo vgcreate vg_1 /dev/sdb
  Volume group "vg_1" successfully created
[nioa@node1 ~]$ sudo vgextend vg_1 /dev/sdc
  Volume group "vg_1" successfully extended
  
  [nioa@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize   PFree
  /dev/sda2  rl lvm2 a--  <11.00g    0
  /dev/sdb      lvm2 ---    3.00g 3.00g
  /dev/sdc      lvm2 ---    3.00g 3.00g
[nioa@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <11.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              2815
  Free PE               0
  Allocated PE          2815
  PV UUID               t2h5eM-WdIS-aL9c-dT44-cSt0-ZKaJ-CWqVrq

  "/dev/sdb" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               vCGPDW-NTtx-OhWO-brDc-z3Lq-lPK5-dFruNa

  "/dev/sdc" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               IFyqab-SVPS-Um0b-0TyP-dhBo-NfJ0-empaIL
```

> créer 3 logical volumes de 1 Go chacun

```
[nioa@node1 ~]$ sudo lvcreate -L 1G vg_1 -n lv_1
  Logical volume "lv_1" created.
[nioa@node1 ~]$ sudo lvcreate -L 1G vg_1 -n lv_2
  Logical volume "lv_2" created.
[nioa@node1 ~]$ sudo lvcreate -L 1G vg_1 -n lv_3
  Logical volume "lv_3" created.

[nioa@node1 ~]$ sudo lvs
  LV   VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root rl   -wi-ao---- 9.79g
  swap rl   -wi-ao---- 1.20g
  lv_1 vg_1 -wi-a----- 1.00g
  lv_2 vg_1 -wi-a----- 1.00g
  lv_3 vg_1 -wi-a----- 1.00g
[nioa@node1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/vg_1/lv_1
  LV Name                lv_1
  VG Name                vg_1
  LV UUID                qC0l9c-No3j-rcnX-mR0M-lBMC-Fsa9-7oRip2
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-25 23:59:49 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/vg_1/lv_2
  LV Name                lv_2
  VG Name                vg_1
  LV UUID                A3RdTX-bM3j-sORW-6aUU-AVvt-llGg-6HbHOA
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-26 00:01:22 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/vg_1/lv_3
  LV Name                lv_3
  VG Name                vg_1
  LV UUID                wNYz7c-1Xz8-7YfJ-LP58-HNde-jq4z-8oFVGP
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-26 00:01:24 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4

  --- Logical volume ---
  LV Path                /dev/rl/swap
  LV Name                swap
  VG Name                rl
  LV UUID                NRwn8z-LORI-uASD-9Ghi-szCg-o5BW-6UlvBb
  LV Write Access        read/write
  LV Creation host, time bastion-ovh1fr.auvence.co, 2021-09-15 11:01:34 +0200
  LV Status              available
  # open                 2
  LV Size                1.20 GiB
  Current LE             308
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/rl/root
  LV Name                root
  VG Name                rl
  LV UUID                mEhdIf-PKNN-XYYC-bSwW-77qc-sR6a-Oyuhj7
  LV Write Access        read/write
  LV Creation host, time bastion-ovh1fr.auvence.co, 2021-09-15 11:01:34 +0200
  LV Status              available
  # open                 1
  LV Size                9.79 GiB
  Current LE             2507
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```

> formater ces partitions en ext4

```
[nioa@node1 ~]$ sudo mkfs  -t ext4 /dev/mapper/vg_1-lv_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: cd116dba-1c5e-4d20-aad4-68e77d566471
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[nioa@node1 ~]$ sudo mkfs  -t ext4 /dev/mapper/vg_1-lv_2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ccdcb8fa-b74e-42e5-8fe6-e9a16f096b95
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[nioa@node1 ~]$ sudo mkfs  -t ext4 /dev/mapper/vg_1-lv_3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 43458156-193f-49c9-b1fb-de6eca803fa5
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

> monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.

```
[nioa@node1 ~]$ sudo mkdir /mnt/part1
[nioa@node1 ~]$ sudo mkdir /mnt/part2
[nioa@node1 ~]$ sudo mkdir /mnt/part3

[nioa@node1 ~]$ sudo mount /dev/mapper/vg_1-lv_1 /mnt/part1
[nioa@node1 ~]$ sudo mount /dev/mapper/vg_1-lv_2 /mnt/part2
[nioa@node1 ~]$ sudo mount /dev/mapper/vg_1-lv_3 /mnt/part3
[nioa@node1 ~]$ mount
[...]
/dev/mapper/vg_1-lv_1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/vg_1-lv_2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/vg_1-lv_3 on /mnt/part3 type ext4 (rw,relatime,seclabel)
[nioa@node1 ~]$ df -h
[...]
/dev/mapper/vg_1-lv_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/vg_1-lv_2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/vg_1-lv_3  976M  2.6M  907M   1% /mnt/part3
```

* Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

```
[nioa@node1 ~]$ cat /etc/fstab
[...]
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/mapper/vg_1-lv_1   /mnt/part1              ext4    defaults        0 0
/dev/mapper/vg_1-lv_2   /mnt/part2              ext4    defaults        0 0
/dev/mapper/vg_1-lv_3   /mnt/part3              ext4    defaults        0 0
[nioa@node1 ~]$ sudo umount /mnt/part1
[nioa@node1 ~]$ sudo umount /mnt/part2
[nioa@node1 ~]$ sudo umount /mnt/part3
[nioa@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounted
```

## III. Gestion de services
### 1. Interaction avec un service existant

* l'unité est démarrée
```
[nioa@node1 ~]$ systemctl is-active firewalld
active
```

* l'unitée est activée (elle se lance automatiquement au démarrage)
```
[nioa@node1 ~]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service

#### A. Unité simpliste

* Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.

```
[nioa@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

* Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.

```
[nioa@node1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
[...]
</ul>
<hr>
</body>
</html>
```

#### B. Modification de l'unité

* Créer un utilisateur web.
```
[nioa@node1 ~]$ sudo useradd web
```
* Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses : 
```
[nioa@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv

[Install]
WantedBy=multi-user.target
```

* Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.

```
[nioa@node1 ~]$ sudo touch /srv/unFichierCompletementOuf
[nioa@node1 ~]$ sudo chown web /srv/unFichierCompletementOuf
```

* Vérifier le bon fonctionnement avec une commande curl

```
[nioa@node1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="unFichierCompletementOuf">unFichierCompletementOuf</a></li>
</ul>
<hr>
</body>
</html>
```